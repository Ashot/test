import csv
import pymysql.cursors

connection = pymysql.connect(host='localhost',
                             user='root',
                             password='1230',
                             db='test',
                             charset='utf8',
                             cursorclass=pymysql.cursors.DictCursor)
							 
student_data = csv.reader(file('student.csv'))
teacher_data = csv.reader(file('teacher.csv'))
class_data = csv.reader(file('class.csv'))
subject_data = csv.reader(file('subject.csv'))
role_data = csv.reader(file('role.csv'))
ths_data = csv.reader(file('teacher_has_subject.csv'))
shs_data = csv.reader(file('student_has_subject.csv'))

with connection.cursor() as cursor:
	cursor.execute("SET FOREIGN_KEY_CHECKS=0")
	cursor.execute("TRUNCATE TABLE student")
	for row in student_data:
		print(row)
		cursor.execute("insert into student(id, name, surname, class_id, \
			position_id) values(%s, %s, %s, %s, %s)", row)
	connection.commit()
	print("Student done")
	
	cursor.execute("TRUNCATE TABLE teacher")
	for row in teacher_data:
		print(row)
		cursor.execute("insert into teacher(id, name, surname, \
			position_id) values(%s, %s, %s, %s)", row)
	connection.commit()
	print("Teacher done")
	
	cursor.execute("TRUNCATE TABLE class")
	for row in class_data:
		print(row)
		cursor.execute("insert into class(id) values(%s)", row)
	connection.commit()
	print("Class done")
	
	cursor.execute("TRUNCATE TABLE subject")
	for row in subject_data:
		print(row)
		cursor.execute("insert into subject(id, name) values(%s, %s)", row)
	connection.commit()
	print("Subject done")
	
	cursor.execute("TRUNCATE TABLE role")
	for row in role_data:
		print(row)
		cursor.execute("insert into role(id, name, rights) values(%s, %s, %s)", row)
	connection.commit()
	print("Role done")
	
	cursor.execute("TRUNCATE TABLE teacher_has_subject")
	for row in ths_data:
		print(row)
		cursor.execute("insert into teacher_has_subject(teacher_id, subject_id, \
			class_id) values(%s, %s, %s)", row)
	connection.commit()
	print("Teacher has subject done")
	
	
	cursor.execute("TRUNCATE TABLE student_has_subject")
	for row in shs_data:
		print(row)
		cursor.execute("insert into student_has_subject(subject_id, student_id, mark) \
			values(%s, %s, %s)", row)
	connection.commit()
	print("Student has subject done")
	
	cursor.close()